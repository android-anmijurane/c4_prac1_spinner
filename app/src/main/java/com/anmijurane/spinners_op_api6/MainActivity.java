package com.anmijurane.spinners_op_api6;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private Spinner spin;
    private EditText numberOne, numberTwo;
    private TextView result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.MakeOper).setOnClickListener(this);
        numberOne = (EditText) findViewById(R.id.Number1);
        numberTwo = (EditText) findViewById(R.id.Number2);
        result = (TextView) findViewById(R.id.ResultOper);

        spin = (Spinner) findViewById(R.id.SpinnerOption);
        String[] opciones = {"sumar", "restar", "multiplicar", "dividir"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this, android.R.layout.simple_spinner_item, opciones);
        spin.setAdapter(adapter);

    }

    @Override
    public void onClick(View v) {
        double num1 = Double.parseDouble(numberOne.getText().toString());
        double num2 = Double.parseDouble(numberTwo.getText().toString());
        String Selec = spin.getSelectedItem().toString();

        if (Selec.equals("sumar")) {
            result.setText("RESULTADO: " + OperSuma(num1, num2));

        } else if (Selec.equals("restar")) {
            result.setText("RESULTADO: " + OperResta(num1, num2));

        } else if (Selec.equals("multiplicar")) {
            result.setText("RESULTADO: " + OperMult(num1, num2));

        } else if (Selec.equals("dividir")) {
            result.setText("RESULTADO: " + OperDiv(num1, num2));

        }
    }

    public double OperSuma(double n1, double n2){
        return n1 + n2;
    }

    public double OperResta(double n1, double n2){
        return n1 - n2;
    }

    public double OperMult(double n1, double n2){
        return n1 * n2;
    }

    public double OperDiv(double n1, double n2){
        return n1 / n2;
    }

}
